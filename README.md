# vue-cli-plugin-route-loader

1. 实现异步加载远程路由 和 打包项目。
2. 支持 webpack 插件， vue-cli 插件等模式
3. 通过 global.loader 可以加载 webpack 打包出来的任意组件模块

## 安装

首先你需要先全局安装 `@vue/cli` 。

然后在你的项目根目录执行以下命令：

```bash
vue add route-loader
或者使用webpack插件
npm i --save-dev vue-cli-plugin-route-loader
```

## 配置

1. 在项目根目录下的文件 `package.json 中 添加 routeLoaderOptions`；

2. 或者在项目根目录下的文件 `vue.config.js` （如果没有请创建）中添加
3. 或者 webpack 插件实例传入参数

## 配置项说明

```javascript
module.exports = {
  pluginOptions: {
    // 该插件对应的配置
    routeLoaderOptions: {
        mode: 0, // 环境配置， 0：开发环境加载插件，1：生产环境加载插, 2：所有环境加载插件，其他情况不加载
        name: "main", // 当前项目名称 默认 main,
        //  默认项目配置 其他参数参考项目配置
        default: {
            publicPath: "", // 资源路径
            versionApi: "", // 版本控制url
            commonChunks: [], // [chunk-common, chunk-vendors],
            bypass: Function | String, // bypass(chunkName, fileType, pathConfig) => url 自定义处理文件路径，
        },
        // 项目配置
        modules: [{
            mode: 0, // 0：开发环境启用，1：生产环境启用，2：所有环境启用，其他情况不加载
            name: "main", // 模块项目名称
            commonChunks: [], // [chunk-common, chunk-vendors],
            publicPath: "", // 资源路径
            versionApi: "", // 版本控制url
            // 加载指定的版本
            // 写死版本号 在非isDevServe 下有效果
            version: "version",
            bypass: Function | String, // 自定义处理文件路径，
        }]
    }
}
```

## 建议 webpack 配置

1. libraryTarget 使用 'jsonp'

## aff 项目本地开发

1. 请注释 common 项目的 html 的 version.js 配置
2. 请修改 common 项目 src/router/index.js 的 load 方法

```js
// 修改前load 方法
loader(routePath);
// 修改后load 方法
global.loader(routePath);
```

## 项目配置事例

<!-- vue.config.js 配置 -->

```js
pluginOptions: {
    routeLoaderOptions: {
        name: "store",
        default: {
            publicPath: 'http://host/path',
            commonChunks: ['chunk-common', 'chunk-vendors.js'],
            versionApi: 'version.js',
        },
        modules: [
            // store 加载 mo 的本地机器代码
            {
                name: 'mo',
                publicPath: 'http://172.24.145.67:8899/',
                bypass: "static/{{fileType}}/{{chunkName}}!pluginNames=name1,name1,name1"
            }
        ]
    }
}
```

## 详细配置说明

| 配置名                    | 类型                  | 描述                                                                                                            |
| ------------------------- | --------------------- | --------------------------------------------------------------------------------------------------------------- |
| mode                      | Number                | 0：开发环境（默认）; 1：生产环境 2：所有环境；其他情况不加载                                                    |
| globalName                | String                | 本插件全局变量名称（默认： loader），通过 window.loader 使用                                                    |
| name                      | String                | 当前项目名称 默认 main;                                                                                         |
| inject                    | false, 'all', 'entry' | false: 不注入启动 loader 代码; 'all'： 每个入口都注入 loader; entry： 每个 html 入口都注入自动运行 loadermain   |
| pages                     | String                | 自定义页面入口 [name].js：是手动指定路由配置入口; src/file: 自动生成入口仅过滤.vue 文件，排除 component[s] 文件 |
| version                   | String                | 此次编译版本                                                                                                    |
| isMain                    | Boolean               | 当前项目是否主模块(默认 false)                                                                                  |
| html                      | Boolean               | 多入口自动生成 html (默认 true)                                                                                 |
| default.publicPath        | String                | 资源路径                                                                                                        |
| default.versionApi        | String                | 版本控制 url                                                                                                    |
| default.commonChunks      | Array                 | 公共模块配置                                                                                                    |
| default.globalVersion     | String                | version js 默认字段 (默认 baseConfig)                                                                           |
| default.stopGlobalVersion | Boolean               | 阻止运行 version js 运行 (默认 true)                                                                            |
| default.preload           | Boolean               | 是否预加载资源 (默认 null)                                                                                      |
| default.bypass            | Function, String      | 自定义处理资文件路径 (默认 null)                                                                                |
| modules                   | Array(module)         | 项目依赖配置                                                                                                    |
| module.mode               | Number                | 0：开发环境（默认）; 1：生产环境 2：所有环境；其他情况不加载当前配置                                            |
| module.name               | String                | 该依赖项目名称                                                                                                  |
| module.version            | String                | 指定该依赖项目版本                                                                                              |
| module.isMain             | Boolean               | 该依赖项目是否主模块                                                                                            |
| module.isDevServe         | Boolean               | 该依赖项目是否开启开发环境(默认 null) ,                                                                         |
| module.publicPath         | String                | 资源路径                                                                                                        |
| module.versionApi         | String                | 版本控制 url                                                                                                    |
| module.commonChunks       | Array                 | 公共模块配置                                                                                                    |
| module.globalVersion      | String                | version js 默认字段 (默认 baseConfig)                                                                           |
| module.stopGlobalVersion  | Boolean               | 阻止运行 version js 运行 (默认 true)                                                                            |
| module.preload            | Boolean               | 是否预加载资源 (默认 null)                                                                                      |
| module.bypass             | Function, String      | 自定义处理资文件路径 (默认 null)                                                                                |
| module.entries            | Object                | 自定义模块文件插件关系                                                                                          |
| module.pluginNames        | Array                 | 指定模块某些插件启用本配置                                                                                      |

1. module 参数会覆盖 default 参数
2. bypass 说明
    1. 当 bypass 为 String 类型支持插值({{}})
    2. 当 module.bypass 携带'!pluginNames=name1,name2,....' 参数是指定模块某些插件启用本配置， 和 module.pluginNames 参数功能一样
    3. 更多插值参数请开发调试查看

## 问题

1. 在多页面下如果出现样式问题，可能是其他模块样式影响了全局， 请刷新当前页面解决

## 更多参数参考插件 config.md 文档
