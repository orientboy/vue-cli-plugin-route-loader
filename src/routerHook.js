
export default function routerResolveHook(router, callback, aliasMap) {
    router.beforeResolve((to, from, next) => {
        const isFn = typeof callback === 'function';
        const globalName = global.RouteLoaderConfig && global.RouteLoaderConfig.globalName || 'loader';
        if (to.matched.length > 0) {
            return isFn ? callback(to, from, next) : next();
        }
        let path = to.path || '';
        let pluginNames = ((to.query || {})._debug_plugin_names_ || '').split(',');
        // 路由映射不能出现 路由参数（/:id/）形式, 以后待优化
        let sourcePath = Array.isArray(aliasMap) && aliasMap.find(item => item.path === path);
        sourcePath = sourcePath && sourcePath.module || path;
        global[globalName](sourcePath, {
            // 通过链接debug 切换 组件资源请求
            pluginNames: pluginNames.length ? pluginNames : null
        }).then(module => {
            if (Array.isArray(module)) module = module[0];
            if (module && module._routeOriginPathFnName) {
                let originPath = module._routeOriginPathFnName.replace(/\_+/g, '_').split('_');
                let params = {}
                path = path.replace(/^\//, '').split('/').map((val, index) => {
                    let originVal = originPath[index];
                    if (originVal && originVal[0] === '$') {
                        originVal[0] = ":";
                        params[originVal.slice(1)] = val;
                        return originVal
                    }
                    return val;
                }).join('/');
                if (path[0] !== '/') path = '/' + path;
                Object.assign(to.params, params);
            };
            if (module) {
                router.addRoutes({
                    path,
                    component: module
                });
                to.matched[0] = {
                    components: {
                        default: module
                    },
                    instances: {
                        default: module
                    },
                    enteredCbs: {
                        default: []
                    }
                };
            }
            return isFn ? callback(to, from, next, module, 'module') : next();
        }).catch(err => {
            return isFn ? callback(to, from, next, err, 'module') : next();
        });
    });
}