/**
 * @file 页面插件（组件）动态加载
 *
 */
import AsyncExec from './asyncExec';
// 缓存项目模块
const TempPluginNames = {};
// 缓存版本号
const TempVersions = {
    // url: {
    //     projectName:
    //     {
    //         version: '11111',
    //         commonChunks: [],
    //         // chunks: [],
    //         // 入口模块记录
    //         // 为了 兼容扩展 pluginName  和 js 文件名不一样
    //         entries: {
    //             app: ["app.hash.js", "app.hash.css"]
    //         }
    //         // 设计基础 路由是具有唯一性的
    //         // 寻找模块: 先通过 项目名快速匹配 ,未匹配到 在通过 所有 entries 匹配 
    //     },
    // },
};
const pathJoin = function (...args) {
    return args.reduce((total, val) => {
        if (!val && val !== 0) val = '';
        if (/^(\w+:)\/\//.test(val)) return val.replace(/\/+$/, '')
        val = val.replace(/(^\/+)|(\/+$)/g, '');
        if (!val) return total;
        return total + "/" + val;
    }, '').replace(/\s+/g, '');
};
// 获取指定插件
const getPluginNames = function (obj = {}) {
    return obj && Array.isArray(obj.pluginNames) && obj.pluginNames.length > 0 && obj.pluginNames;
}
/**
 * 解析参数
 * @param {string} routePath
 * @param {object} options
 * @returns
 */
const resolveOption = function (routePath, options) {
    options = options || {};
    routePath = routePath.replace(/^\/|\s/g, '');
    const RouteLoaderConfig = global.RouteLoaderConfig || {};
    const NODE_ENV = RouteLoaderConfig.NODE_ENV || 'production';
    const projectName = routePath.split('/')[0].replace(/[\_\-]/g, '');
    const pluginName = options.pluginName || routePath.replace(/[\_\-]/g, '').replace(/[\/]/g, '_'); // /-_a/ => a => a_;
    const curModule = (RouteLoaderConfig.modules || []).find(item => item.name === projectName) || { __noModule: true };
    let pluginNames = getPluginNames(options) || getPluginNames(curModule);
    let bypass = curModule.bypass;
    let preload = curModule.preload;
    let isDevServe = curModule.isDevServe && NODE_ENV !== 'production';
    let defaultOptions = null;
    if (bypass && typeof bypass === 'string') {
        let paths = bypass.split('!pluginNames=');
        let names = (paths[1] || '').replace(/\s+/, '').split(',');
        bypass = paths[0];
        pluginNames = pluginNames || (names.indexOf('*') === -1 && names);
    }
    if (!pluginNames || !pluginNames.length) {
        defaultOptions = {
            ...RouteLoaderConfig.default,
            ...curModule
        };
       
    } else if (pluginNames.indexOf(pluginName) !== -1) {
        defaultOptions = {
            ...RouteLoaderConfig.default,
            ...curModule
        };
        isDevServe = false;
    } else {
        defaultOptions = {
            ...RouteLoaderConfig.default,
        }
        bypass = preload = null;
    }
    if (isDevServe) {
        defaultOptions.publicPath = '/';
    }
    if (defaultOptions.stopGlobalVersion == null) {
        defaultOptions.stopGlobalVersion = true;
    }
    // 优先级别, 版本号配置 > options 配置 > 默认配置
    // 特殊权限 指定版本后 options 配置 > 默认配置 > 版本号配置
    return {
        NODE_ENV,
        isDevServe,
        defaultOptions,
        options,
        routePath,
        projectName,
        // 插件模块js文件名
        // 当versions 版本 不存在时回退到pluginFileName
        pluginName,
        publicPath: options.publicPath || defaultOptions.publicPath,
        versionApi: options.versionApi || defaultOptions.versionApi,
        commonChunks: options.commonChunks || defaultOptions.commonChunks,
        // 内置写死版本号
        version: options.version || defaultOptions.version,
        entries: options.entries || defaultOptions.entries,
        libraryTarget: options.libraryTarget || defaultOptions.libraryTarget || RouteLoaderConfig.libraryTarget,
        globalVersion: options.globalVersion || defaultOptions.globalVersion || 'baseConfig', // 'baseConfig',  // 针对 aff 框架 阻止运行
        stopGlobalVersion: options.stopGlobalVersion == null ? defaultOptions.stopGlobalVersion : options.stopGlobalVersion, // true,  // 针对 aff 框架 阻止运行
        bypass: bypass == null ? defaultOptions.bypass : bypass,
        preload: preload == null ? defaultOptions.preload : preload,
        // 是否启动入口
        startEntry: !!options.startEntry,
        __noModule: !!curModule.__noModule
    };
};
// 兼容aff 
const TempVersionsScript = {};
// 可以不依赖 本地框架 直接使用线上框架 开发
// 通过配置 来 选择 选择不同框架运行
const loadVersionsScript = async function (resolvedOption) {
    if (resolvedOption.isDevServe || !resolvedOption.versionApi) return;
    const url = pathJoin(resolvedOption.publicPath, resolvedOption.versionApi);
    if (TempVersions[url]) {
        return TempVersions[url][resolvedOption.projectName];
    }
    const handleTempVersions = function (data) {
        let versions = data.versions || {};
        let hasVersion = false;
        Object.keys(versions).forEach((key) => {
            const value = versions[key];
            hasVersion = true;
            if (!value || typeof value === 'string') {
                versions[key] = {
                    version: value
                };
            }
        });
        return hasVersion ? versions : null;
    }
    return AsyncExec.syncStart(url, function (resolve, reject) {
        // 使用fetch 获取版本号
        if (!/\.js$/i.test(url)) {
            fetch(url + '?t=' + Date.now(), {
                cache: 'no-cache',
                mode: 'cors',
            }).then((res) => res.json()).then((res) => {
                TempVersions[url] = handleTempVersions(res);
                resolve(TempVersions[url] && TempVersions[url][resolvedOption.projectName]);
            }).catch((err) => {
                resolve(TempVersions[url] && TempVersions[url][resolvedOption.projectName]);
            });
        }
        // 兼容 aff 老框架
        if (!TempVersionsScript[url + '?publicPath']) {
            TempVersionsScript[url + '?publicPath'] = window.publicPath;
        }
        // 兼容 老框架
        if (resolvedOption.stopGlobalVersion) {
            // 清除线上common代码影响本地common
            // 阻止version.js自执行
            window.publicPath = '//stop-common-script-load';
        }
        const script = document.createElement('script');
        script.onload = () => {
            // 等待执行
            setTimeout(() => {
                document.head.removeChild(script);
                const globalVersion = global[resolvedOption.globalVersion];
                TempVersions[url] = handleTempVersions(typeof globalVersion === 'function' ? globalVersion(resolvedOption) : globalVersion);
                resolve(TempVersions[url] && TempVersions[url][resolvedOption.projectName]);
                // 兼容aff
                window.publicPath = TempVersionsScript[url + '?publicPath'];
            });
        };
        script.onerror = (err) => {
            document.head.removeChild(script);
            resolve(TempVersions[url] && TempVersions[url][resolvedOption.projectName]);
            // 兼容aff
            window.publicPath = TempVersionsScript[url + '?publicPath'];
        };
        script.src = url + '?t=' + Date.now();
        document.head.appendChild(script);
    });
};

const loadScript = async function (url, isMain) {
    if (!url) return;
    if (!/\.js$/i.test(url)) url += '.js';
    if (TempPluginNames[url]) return;
    return AsyncExec.syncStart(url, function (resolve, reject) {
        const script = document.createElement('script');
        script.onload = () => {
            document.head.removeChild(script);
            if (!isMain) TempPluginNames[url] = true;
            resolve(null);
        };
        script.onerror = (err) => {
            document.head.removeChild(script);
            reject(err);
        };
        script.src = url;
        document.head.appendChild(script);
    });
};

const preloadScript = async function (url) {
    if (!url) return;
    if (!/\.js$/i.test(url)) url += '.js';
    AsyncExec.syncOnce(url + '?preload', function () {
        const link = document.createElement('link');
        link.rel = 'preload';
        link.as = 'script';
        link.crossorigin = true;
        link.onload = () => {
            document.head.removeChild(link);
        };
        link.onerror = () => {
            // 预加载失败不在重试
            document.head.removeChild(link);
        };
        link.href = url;
        document.head.appendChild(link);
    })
};

const loadStyle = async function (url) {
    if (!url) return;
    if (!/\.css$/i.test(url)) url += '.css';
    // if (TempPluginNames[url]) return;
    AsyncExec.syncOnce(url, function (resolve, reject) {
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        // link.onload = () => {
        //     TempPluginNames[url] = true;
        // };
        link.onerror = () => {
            reject();
            document.head.removeChild(link);
        }
        link.href = url;
        document.head.appendChild(link);
    })
};

const loadMain = async function (url, pluginName, moduleConfig) {
    if (!url) return;
    if (!/\.js$/i.test(url)) url += '.js';
    if (TempPluginNames[url]) return TempPluginNames[url];
    const isJsonp = pluginName && (!moduleConfig.libraryTarget || moduleConfig.libraryTarget === 'jsonp');
    // jsonp 加锁 模式
    // 出现global[pluginName] 定义重复问题
    return AsyncExec[isJsonp ? 'syncStep' : 'async'](url, (resolve, reject, next) => {
        // 是否运行
        let isJsonpRuned = false;
        let backFn = global[pluginName] || function () { };
        if (isJsonp) {
            try {
                Object.defineProperty(global, pluginName, {
                    configurable: true,
                    enumerable: false,
                    get() {
                        return function (exports) {
                            if (!exports) exports = {};
                            TempPluginNames[url] = exports.default;
                            isJsonpRuned = true;
                            console.log(TempPluginNames[url], `${pluginName} ----------`);
                            resolve(exports.default || {});
                            // 执行销毁
                            Object.defineProperty(global, pluginName, {
                                configurable: true,
                                enumerable: false,
                                writable: true,
                                value: backFn
                            });
                            next && next();
                        }
                    },
                    set() { }
                });

            } catch (err) {
                global[pluginName] = function (exports) {
                    if (!exports) exports = {};
                    TempPluginNames[url] = exports.default;
                    isJsonpRuned = true;
                    console.log(TempPluginNames[url], `${pluginName} ----------`);
                    resolve(exports.default || {});
                    // 执行销毁
                    global[pluginName] = backFn
                    next && next();
                };
            }
        }
        loadScript(url, true).then(() => {
            // 兼容 pluginName 或js返回定义函数不匹配问题
            // 延迟
            setTimeout(() => {
                resolve(global[pluginName]);
                if (!isJsonp) {
                    TempPluginNames[url] = global[pluginName];
                }
                // 执行销毁
                if (isJsonp && !isJsonpRuned) {
                    try {
                        // 执行销毁
                        Object.defineProperty(global, pluginName, {
                            configurable: true,
                            enumerable: false,
                            writable: true,
                            value: backFn
                        });
                    } catch (error) {
                        global[pluginName] = backFn
                    }
                    next && next();
                }
            })
        }, (err) => {
            reject(err);
            if (isJsonp) {
                try {
                    // 执行销毁
                    Object.defineProperty(global, pluginName, {
                        configurable: true,
                        enumerable: false,
                        writable: true,
                        value: backFn
                    });
                } catch (error) {
                    global[pluginName] = backFn
                }
                next && next();
            }
        });
    });
};

const getSourcUrl = function (chunkName, fileType, pathConfig) {
    const bypass = pathConfig.bypass;
    const type = typeof bypass;
    if (type === 'function') {
        return pathJoin(pathConfig.publicPath, bypass(chunkName, fileType, pathConfig));
    }
    if (type === 'string') {
        return pathJoin(pathConfig.publicPath, bypass.replace(/[\s\?]+/g, '').replace(/(\{\{)([^\{\}\/]+)(\}\})/g, function (match, reg, reg1, reg2) {
            if (reg1 === 'chunkName') return chunkName;
            if (reg1 === 'fileType') return fileType;
            return reg1.split('.').reduce((res, key) => {
                return res[key];
            }, pathConfig);
        }));
    }
    return pathJoin(pathConfig.publicPath, pathConfig.isDevServe ? "" : pathConfig.projectName, pathConfig.version, `static/${fileType}/${chunkName}`)
}

// 自动注入html
const injectConfig = function () {
    console.log('注入入口启动JS：已经运行');
    main.injectModule = function (moduleName, config) {
        if (!config || !moduleName) return;
        const loaderConfig = global.RouteLoaderConfig;
        if (loaderConfig.modules.some(item => item.name === moduleName)) {
            throw new Error(`this The ${moduleName} Project Module is existed`);
        }
        config.name = moduleName;
        loaderConfig.modules.push(config);
    };
    try {
        Object.defineProperty(global, 'RouteLoaderConfig', {
            configurable: false,
            enumerable: false,
            get() {
                return global.__RouteLoaderConfig || { 'NODE_ENV': 'production', modules: [] };
            },
            set() { }
        });
    } catch (error) {
        console.log(error);
    }
    try {
        const globalName = global.RouteLoaderConfig && global.RouteLoaderConfig.globalName || 'loader';
        Object.defineProperty(global, globalName, {
            configurable: false,
            enumerable: false,
            get() {
                return main;
            },
            set() { }
        });
    } catch (error) {
        console.log(error);
    }
    if (!Object.isFrozen(global.RouteLoaderConfig)) {
        global.RouteLoaderConfig.modules = global.RouteLoaderConfig.modules || [];
        Object.freeze(global.RouteLoaderConfig);
    }
}
injectConfig();

// 没有匹配回退所有版本匹配， 
const matchAllEntries = function (pluginName, TempVersions) {
    if (!pluginName) return;
    let projectName;
    let versionConfig = Object.values(TempVersions).find(versions => {
        projectName = versions && Object.keys(versions).find(key => {
            if (Object.prototype.toString.call(versions[key] && versions[key].entries) !== '[object Object]') return;
            return Object.keys(versions[key].entries).find(key => {
                if (key.indexOf('$') > -1) {
                    // key => name_$id_
                    // pluginName => name_name_
                    // reg => /name_[A-Za-z0-9\]/
                    // 严格匹配
                    let bool = new RegExp('^' + key.replace(/\$[^_\$]*/g, '\[A-Za-z0-9\]+') + '$', 'i').test(pluginName);
                    if (bool) pluginName = key;
                    return bool;
                }
                return pluginName === key
            });
        });
        return projectName;
    })
    // 匹配到全局entry
    if (projectName && versionConfig && versionConfig[projectName]) {
        const RouteLoaderConfig = global.RouteLoaderConfig || {};
        const modules = RouteLoaderConfig.modules || [];
        return {
            commonChunks: versionConfig[projectName].commonChunks || (modules.find(item => item.name === projectName) || {}).commonChunks,
            main: versionConfig[projectName].entries[pluginName] || pluginName,
            version: versionConfig[projectName].version,
            pluginName,
            projectName,
            libraryTarget: versionConfig[projectName].libraryTarget || RouteLoaderConfig.libraryTarget
        }
    }
}
/**
 *入口
 * @param {string} routePath // http:// | /path/path
 * @param {object} options
 * @returns
 */
export default async function main(routePath, options) {
    const resolvedOption = resolveOption(routePath, options);
    const projectName = resolvedOption.projectName;
    if (!projectName) return;
    const NODE_ENV = resolvedOption.NODE_ENV;
    const pluginName = resolvedOption.pluginName;
    const versionConfig = (await loadVersionsScript(resolvedOption).catch(err => null)) || {};
    const getModuleVersion = function () {
        // versionConfig 是远程模块配置
        const projectVersion = !resolvedOption.isDevServe && (resolvedOption.version ? {
            /**指定版本入口*/
            // 权重高于 远程版本配置
            version: resolvedOption.version,
            commonChunks: resolvedOption.commonChunks,
            entries: resolvedOption.entries
        } : versionConfig.version && versionConfig);
        const libraryTarget = (projectVersion && projectVersion.libraryTarget) || resolvedOption.libraryTarget;
        const commonChunks = (projectVersion && projectVersion.commonChunks) || resolvedOption.commonChunks;
        const backMain = resolvedOption.entries && resolvedOption.entries[pluginName] || [`${pluginName}.js`, `${pluginName}.css`];
        // isDevServe环境模块或者没有项目版本配置
        if (!projectVersion) {
            return {
                commonChunks: resolvedOption.commonChunks,
                main: backMain,
                // 版本号配置存在时 除了本地开发 其他不能注入版本
                version: resolvedOption.isDevServe && resolvedOption.version || '',
                pluginName,
                projectName,
                // 开发环境且没有配置项目默认开发环境配置
                isDevServe: NODE_ENV !== 'production' && (resolvedOption.isDevServe || resolvedOption.__noModule)
            }
        }
        if (projectVersion.entries) {
            // 表示单页面 直接返回 不受 pluginName 限制
            if (typeof projectVersion.entries === 'string' || Array.isArray(projectVersion.entries)) {
                return {
                    commonChunks,
                    main: projectVersion.entries,
                    version: projectVersion.version,
                    pluginName: pluginName || 'index',
                    projectName,
                    libraryTarget
                }
            }
            // 存在多页面配置
            // 找到插件配置
            if (projectVersion.entries[pluginName]) {
                return {
                    commonChunks,
                    main: projectVersion.entries[pluginName],
                    version: projectVersion.version,
                    pluginName,
                    projectName,
                    libraryTarget
                }
            }
            // 匹配自定义配置关系
            if (resolvedOption.entries && resolvedOption.entries[pluginName]) {
                return {
                    commonChunks,
                    main: backMain,
                    version: projectVersion.version,
                    pluginName,
                    projectName,
                    libraryTarget
                }
            }
            // 但是没有匹配成功 全局查找
            const matchProject = matchAllEntries(pluginName, [projectVersion]) || matchAllEntries(pluginName, TempVersions)
            if (matchProject) {
                return matchProject;
            }
            if (pluginName) {
                return {
                    commonChunks,
                    main: backMain,
                    version: projectVersion.version,
                    pluginName,
                    projectName,
                    libraryTarget
                }
            }
            // 没有entries 配置
        } else {
            // 正式环境入口文件 如 index: index.hash.js 生产环境切换版本会有问题
            return {
                commonChunks,
                main: backMain,
                version: projectVersion.version,
                pluginName,
                projectName,
                libraryTarget
            }
        }
    }
    const moduleConfig = getModuleVersion();
    // 待优化
    // 没有找到默认 到 本地开发环境
    if (!moduleConfig) return;
    const pathConfig = { ...resolvedOption, ...moduleConfig };
    const commonChunks = moduleConfig.commonChunks || [];
    let mainFiles = moduleConfig.main;
    // 配置文件特殊输出
    if (mainFiles === true) mainFiles = [moduleConfig.pluginName];
    if (!Array.isArray(mainFiles)) mainFiles = [mainFiles];
    // 兼容去重
    mainFiles = Array.from(new Set(mainFiles))
    // 加载css
    for (let index = 0, len = commonChunks.length; index < len; index++) {
        const chunkName = moduleConfig.commonChunks[index];
        if (!chunkName) continue;
        if (!/\.js$/i.test(chunkName)) {
            const url = getSourcUrl(chunkName, 'css', pathConfig);
            loadStyle(url).catch(err => console.log(err));
        } else if (pathConfig.preload && !/\.css$/i.test(chunkName)) {
            // 预加载 js
            const url = getSourcUrl(chunkName, 'js', pathConfig);
            preloadScript(url).catch(err => console.log(err));
        }
    }
    // 加载 pluginName css
    for (let index = 0, len = mainFiles.length; index < len; index++) {
        const chunkName = mainFiles[index];
        if (!chunkName) continue;
        if (!/\.js$/i.test(chunkName)) {
            const url = getSourcUrl(chunkName, 'css', pathConfig);
            loadStyle(url).catch(err => console.log(err));
        }
    }
    // 加载common js
    for (let index = 0, len = commonChunks.length; index < len; index++) {
        const chunkName = moduleConfig.commonChunks[index];
        if (!chunkName) continue;
        if (!/\.css$/i.test(chunkName)) {
            const url = getSourcUrl(chunkName, 'js', pathConfig);
            await loadScript(url).catch(err => console.log(err));
        }
    }
    // 主模块js
    const mainPluginFileNames = mainFiles.filter(file => !/\.css$/i.test(file)) || [moduleConfig.pluginName];
    const result = [];
    for (let index = 0, len = mainPluginFileNames.length; index < len; index++) {
        const chunkName = mainPluginFileNames[index];
        const url = getSourcUrl(chunkName, 'js', pathConfig);
        const comp = await loadMain(url, moduleConfig.pluginName, moduleConfig).catch(err => null);
        // 如果是 name_$name => /name/:name 路由
        if (comp && `${chunkName}`.indexOf('$') !== -1) {
            comp._routeOriginPathFnName = chunkName;
        }
        result.push(comp);
    }
    return result.length > 1 ? result : result[0];
}
