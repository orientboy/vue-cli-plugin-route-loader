
const fs = require("fs");
const path = require("path");
const glob = require("glob");
const HtmlWebpackPlugin = require("html-webpack-plugin");

// getPages
const getPages = function (name = "", options = {}) {
    if (!options.pages) return {};
    const pages = options.pages;
    const entry = {};
    const root = options.root || process.cwd();
    if (/\.jsx?$|\.tsx?$/i.test(pages)) {
        const url = path.join(root, pages);
        delete require.cache[url]
        let routes = require(url);
        if (routes.__esModule) routes = routes.default;
        routes.forEach(item => {
            let component = item.component;
            if (typeof component !== 'string') return;
            let routeName = item.path || 'index';
            // routeName = name + (routeName[0] === '/' ? '' : '/') + routeName;
            routeName = routeName.replace(/\/\:+|^\:+/g, '_$').replace(/\/+/g, '_').replace(/\:+|^_+/g, '');
            Object.keys(options.alias).some((key) => {
                if (component.indexOf(key+ '/') === 0) {
                    component = component.replace(key, options.alias[key]);
                    return true;
                }
            })
            entry[routeName] = path.resolve(root, component);
        })
    } else {
        const files = glob.sync(`${pages}/**/*.vue`, { root: options.root });
        files.forEach((file) => {
            if (/components?\//i.test(file)) return;
            let routeName = file.replace(new RegExp(`^${pages}\/\?`, 'i'), '').replace(/\.vue$|[\-\_]/gi, '');
            routeName = name + (routeName[0] === '/' ? '' : '/') + routeName;
            routeName = routeName.replace(/\/\:|^\:/g, '_$').replace(/\/+/g, '_').replace(/\:+|^_+/g, '');
            entry[routeName] = path.resolve(root, file);
        });
    }
    return entry;
};
// webpack config
const mergeWepackConfig = function (compiler, options = {}) {
    const name = `${options.name || "main"}`;
    const jsonpFunction = options.jsonpFunction || `webpackJsonp${name.replace(name[0], name[0].toUpperCase())}`;
    const output = compiler.options.output;
    // 根据 不同 libraryTarget 实现不同的 script 注入调用
    // chunkLoadingGlobal 5.0  的  jsonpFunction
    const config = {
        library: output.library || "[name]",
        libraryTarget: output.libraryTarget || "jsonp",
        jsonpFunction,
        chunkLoadingGlobal: jsonpFunction,
    }
    compiler.options.output = Object.assign(output, config);
    config.root = compiler.options.context;
    config.filename = output.filename;
    return config;
};
// 获取模块插件名称
const getNameFromHtmlPlugin = function (htmlPlugin) {
    if (htmlPlugin.__pluginConstructorName === 'HtmlWebpackPlugin' || htmlPlugin.constructor.name === 'HtmlWebpackPlugin' || htmlPlugin instanceof HtmlWebpackPlugin) {
        const __pluginName = htmlPlugin.__pluginName || '';
        return __pluginName.substr(__pluginName.indexOf("-") + 1) || htmlPlugin.options?.filename?.replace(/\.html?$/i, '')
    }
    return false;
}
// 动态生产多入口配置
const initPages = function (compiler, options = {}, injectConfigFileName) {
    let entry = compiler.options.entry;
    if (typeof entry === "string" || Array.isArray(entry)) {
        entry = entry.length ? {
            index: entry,
        } : {};
    }
    options.alias = compiler.options.resolve.alias || {};
    entry = Object.assign(entry, getPages(options.name, options));
    // 生成html 模板
    const htmlPlugins = compiler.options.plugins.filter(getNameFromHtmlPlugin);
    const htmlPluginNames = htmlPlugins.map(getNameFromHtmlPlugin);
    let template = htmlPlugins[0] && (htmlPlugins[0].options.template || '').split('!')[1];
    if (process.env.NODE_ENV === "production" && template && options.html) {
        Object.keys(entry).forEach(key => {
            if (htmlPluginNames.indexOf(key) !== -1) return;
            let newTemplate = template.replace(/([\/\\])[^\/\\]*$/, `$1${key}.html`);
            if (!fs.existsSync(newTemplate)) {
                newTemplate = template;
            }
            const newHtml = new HtmlWebpackPlugin({
                ...htmlPlugins[0].options,
                ...(htmlPlugins[0].__pluginArgs || [])[0],
                template: newTemplate,
                filename: `${key}.html`,
                chunks: ['chunk-vendors', 'chunk-common', key],
            });
            newHtml.__pluginConstructorName = 'HtmlWebpackPlugin';
            newHtml.__pluginName = `html-${key}`;
            compiler.options.plugins.push(newHtml);
            htmlPluginNames.push(key);
        });
    }
    if (options.inject === "all" && injectConfigFileName) {
        // 作为辅助工具
        Object.keys(entry).forEach((key) => {
            if (Array.isArray(entry[key])) {
                entry[key].unshift(injectConfigFileName);
            } else {
                entry[key] = [injectConfigFileName, entry[key]];
            }
        });
    }
    // else 作为启动入口 可以控制主程序版本
    compiler.options.entry = entry;
}
// 处理options 参数
const mergeOptions = function(options, NODE_ENV) {
    const mode = options.mode || 0;
    const name = options.name || "main";
    const modules = options.modules || [{name}];
    const isMain = !!options.isMain;
    const isProd = NODE_ENV === 'production';
    const mainProject = modules.find(item => item.isMain);
    let inject = options.inject;
    let curProject = modules.find(item => item.name === name);
    if (!curProject) {
        curProject = {name};
        modules.push(curProject);
    }
    // isDevServe  外部指定可控制 当前模块是本地代码还是线上代码 
    if (curProject.isDevServe == null) {
        curProject.isDevServe = !isProd;
    }
    // 默认配置
    if (inject == null && isMain) {
        // 当前模块是主模块
        inject = 'entry';
    } else if (inject == null && mainProject) {
        // 当前模块是子模块 且依赖线上主模块
        inject = isProd  ? false :  'entry';
    }  else if (inject == null) {
        // 当前模块是子模块 但依赖本地主模块包
        inject = isProd  ? false :  'entry';
    } else if (isMain && !inject) {
        // inject 指定错误矫正
        inject =  'entry';
    }
    const config = {
        NODE_ENV,
        mode,
        name,
        inject,
        modules,
        isMain,
        html: options.html || options.html == null,
        globalName: options.globalName || 'loader'
    };
    return Object.assign(options, config);
}
// 不同环境是否允许运行
const isRunEnv =  function(mode, NODE_ENV = process.env.NODE_ENV) {
    return  (
        // 线上
        (NODE_ENV === "production" && mode === 1) ||
        // 仅本地
        (NODE_ENV !== "production" && mode === 0) ||
        // 本地 和 线上
        mode === 2
    )
}
function RouteLoaderPlugin(apiOrmoduleOptions, webpackOptions) {
    // 是webpack 配置插件
    if (this instanceof RouteLoaderPlugin) {
        this.NODE_ENV = process.env.NODE_ENV;
        this.options = mergeOptions(apiOrmoduleOptions || {}, this.NODE_ENV);
        // 是否允许插件
        this.isAllowRun = isRunEnv(this.options.mode, this.NODE_ENV);
        // 优化在本地开发时可以直接注入devServe配置
        return;
    }
    // 是vue-cli 插件
    if (apiOrmoduleOptions.configureWebpack) {
        const options =
            (webpackOptions.pluginOptions && webpackOptions.pluginOptions.routeLoaderOptions) ||
            process.VUE_CLI_SERVICE.pkg.routeLoaderOptions ||
            {};
        apiOrmoduleOptions.chainWebpack((webpackConfig) => {
            webpackConfig.plugin("RouteLoaderPlugin").use(RouteLoaderPlugin, [options]);
        });
    }
}
RouteLoaderPlugin.prototype.apply = function (compiler) {
    if (!this.isAllowRun) return;
    // 注入类型
    // inject = false | 'entry' | 'all';
    // 0. 不注入loader
    // 1. 在html中作为启动入口 注入loader
    // 2. 在所有模块注入loader
    this.options = Object.assign(this.options, mergeWepackConfig(compiler, this.options));
    const mainProject = this.options.modules.find((item) => item.isMain);
    const mainProjectName = (mainProject && mainProject.name) || this.options.name;
    const injectType = this.options.inject;
    const isProd = this.NODE_ENV === "production";
    const injectConfigFileName = injectType && injectConfig(compiler.options.output.path, this.options, this.NODE_ENV);
    initPages(compiler, this.options, injectConfigFileName);
    compiler.hooks.afterPlugins.tap(this.constructor.name, (compiler) => {
        compiler.options.plugins.forEach((plugin) => {
            if (injectType === "entry" && getNameFromHtmlPlugin(plugin)) {
                plugin.options.chunks = [];
            }
        });
    });
    compiler.hooks.afterCompile.tap(this.constructor.name, (compilation) => {
        const htmlWebpackPluginAlterAssetTags  = compilation.hooks.htmlWebpackPluginAlterAssetTags
        htmlWebpackPluginAlterAssetTags && htmlWebpackPluginAlterAssetTags.tap(this.constructor.name, (htmlPluginData) => {
            if (injectType !== "entry") return htmlPluginData;
            // 注入启动代码
            let pluginName = getNameFromHtmlPlugin(htmlPluginData.plugin);
            let entryChunks = (compilation.entrypoints.get(pluginName) || {}).chunks || [];
            let entries = null;
            let commonChunks = null;
            if (!mainProject) {
                commonChunks = [];
                // 没有依赖线上主模块
                entryChunks.forEach((chunk) => {
                    const filenames = chunk.files.map((file) => file.split("/").pop());
                    if (pluginName === chunk.id) {
                        entries = {
                            [pluginName]: filenames
                        };
                        return;
                    }
                    commonChunks.push(...filenames);
                });
            } else {
                // 依赖线上主模块， 需要注入线上启动模块
                pluginName = mainProject.pluginName;
                entries = mainProject.entries;
            }
            if (!pluginName) return htmlPluginData;
            htmlPluginData.body = htmlPluginData.body || [];
            htmlPluginData.body.push({
                tagName: "script",
                closeTag: true,
                attributes: {
                    type: 'text/javascript',
                    src: path.join((compiler.options.output.publicPath || './'), 'routeLoaderWebpackPlugin.js'),
                },
            });
            htmlPluginData.body.push({
                tagName: "script",
                closeTag: true,
                attributes: {
                    type: "text/javascript",
                },
                innerHTML: `
                    window[window.RouteLoaderConfig && window.RouteLoaderConfig.globalName || 'loader']("${mainProjectName}", {
                        pluginName:"${pluginName}",
                        // 生成环境不注入
                        version: "${isProd ? "" : this.options.version || ""}",
                        commonChunks:${commonChunks ? JSON.stringify(Array.from(new Set(commonChunks))) : null},
                        entries:${entries ? JSON.stringify(entries) : null},
                        // 是否是启动入口
                        startEntry: true,
                    });`,
            });
            return htmlPluginData;
        });
    });
    // 生成当前 info
    compiler.hooks.afterEmit.tap(this.constructor.name, (compilation) => {
        if (injectType === "entry") {
            compiler.outputFileSystem.writeFile(
                injectConfigFileName,
                fs.readFileSync(injectConfigFileName, { encoding: "utf8" }),
                (err) => {
                    if (err) return;
                }
            );
        }
        if (!isProd) return;
        const commonChunksSet = new Set();
        const pagesConfig = {};
        compilation.entrypoints.forEach((entry) => {
            (entry.chunks || []).forEach((chunk) => {
                if (entry.name === chunk.id) {
                    pagesConfig[chunk.id] = Array.from(
                        new Set((chunk.files || []).map((file) => file.split("/").pop()))
                    );
                    // 特殊输出 减少字符
                    if (pagesConfig[chunk.id].length > 1) {
                        const newArr = Array.from(new Set(pagesConfig[chunk.id].map(item => item.replace(/\.js$|\.css$/i, ''))))
                        if (newArr.length === 1 && chunk.id === newArr[0]) {
                            pagesConfig[chunk.id] = true;
                        }
                    }
                    return;
                }
                (chunk.files || []).forEach((file) => {
                    commonChunksSet.add(file.split("/").pop());
                });
            });
        });
        //    输出info
        fs.writeFileSync(
            path.join(compilation.outputOptions.path, "./info.config.json"),
            JSON.stringify({
                name: this.options.name,
                isMain: this.options.isMain,
                version: this.options.version,
                commonChunks: Array.from(commonChunksSet),
                entries: pagesConfig,
                fileRule: this.options.filename,
                jsonpFunction: this.options.jsonpFunction,
                globalName: this.options.globalName || 'loader',
                libraryTarget: this.options.libraryTarget
            }, null, 4),
            { encoding: "utf8", flag: "w+" }
        );
    });
};

// 需要 自动注入html 是否自动加载
function injectConfig(dest, options, NODE_ENV) {
    options = {
        NODE_ENV,
        name: options.name,
        isMain: options.isMain,
        modules: options.modules.filter(item => isRunEnv(item.mode || 0, NODE_ENV)),
        fileRule: options.filename,
        libraryTarget: options.libraryTarget,
        default: options.default,
        version: options.version,
        globalName: options.globalName || 'loader'
    };
    options = JSON.stringify(
        options || {},
        function (key, value) {
            if (typeof value === "function") return value.toString();
            return value;
        },
    );
    const content = fs
        .readFileSync(path.join(__dirname, '../dist/injectRouteLoader.umd.js'), { encoding: 'utf8' })
        .replace('global.__RouteLoaderConfig', options);
    if (!fs.existsSync(dest)) fs.mkdirSync(dest, { recursive: true });
    dest = path.join(dest, "./routeLoaderWebpackPlugin.js");
    fs.writeFileSync(dest, content, { encoding: "utf8", flag: "w+" });
    return dest;
}
module.exports = RouteLoaderPlugin;
