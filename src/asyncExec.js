
// Promise 
// 1.  a 执行完 后边 都取a的结果
// 2. a,b,c 顺序执行
// 3. a,b,c 同步执行 取 最早
const TempPromiseRecord = {};
export default {
    asyncEnd() { },
    asyncAll(name, fn) { },
    asyncRace(name, fn) { },
    async(name, fn) {
        if (!name || !fn) return;
        return new Promise(fn);
    },
    syncOnce(name, fn) {
        if (!name || !fn) return;
        const key = name + '@syncOnce';
        if (TempPromiseRecord[key]) return;
        TempPromiseRecord[key] = true;
        fn(function resolve() {
            // TempPromiseRecord[key] = null;
        }, function reject() {
            TempPromiseRecord[key] = null;
        });
    },
    // 顺序执行
    // 优化 isJumpReject 和 下一次执行待上一次结果
    syncStep(name, fn, isJumpReject) {
        if (!name || !fn) return;
        return new Promise((resolve, reject) => {
            const key = name + '@syncStep';
            const loadingKey = key + '@Loading';
            if (!TempPromiseRecord[key]) {
                TempPromiseRecord[key] = [fn]
            } else {
                TempPromiseRecord[key].push(fn)
            }
            // 阻止运行
            if (TempPromiseRecord[loadingKey]) return;
            TempPromiseRecord[loadingKey] = true;
            const execFn = function () {
                const next = Array.isArray(TempPromiseRecord[key]) && TempPromiseRecord[key].shift();
                if (!next || !TempPromiseRecord[key].length) {
                    TempPromiseRecord[loadingKey] = null;
                    TempPromiseRecord[key] = null;
                }
                next && next(resolve, reject, execFn);
            };
            execFn();
        });
    },
    // 只执行第一个，后续都取第一个结果
    syncStart(name, fn) {
        if (!name || !fn) return;
        return new Promise((resolve, reject) => {
            const key = name + '@syncStart';
            const loadingKey = key + '@Loading';
            if (!TempPromiseRecord[key]) {
                TempPromiseRecord[key] = [{ resolve, reject }]
            } else {
                TempPromiseRecord[key].push({ resolve, reject })
            }
            // 阻止运行
            if (TempPromiseRecord[loadingKey]) return;
            TempPromiseRecord[loadingKey] = true;
            const execFn = function (data, execMethod) {
                Array.isArray(TempPromiseRecord[key]) && TempPromiseRecord[key].forEach(exec => {
                    exec[execMethod](data);
                });
                TempPromiseRecord[key] = null;
                TempPromiseRecord[loadingKey] = null;
            }
            fn(function (data) {
                return execFn(data, 'resolve')
            }, function (error) {
                return execFn(error, 'reject')
            });
        });
    }
}