
## 配置

1. 在项目根目录下的文件 `package.json 中 添加 routeLoaderOptions`；
   或者
2. 在项目根目录下的文件 `vue.config.js` （如果没有请创建）中添加：

## 所有配置项说明
```javascript

module.exports = {
  pluginOptions: {
    // 该插件对应的配置
    routeLoaderOptions: {
        globalName: 'loader' // 本插件全局变量名称（默认： loader），通过 window.loader 使用   
        mode: 0, // 环境配置， 0：开发环境加载插件，1：生产环境加载插, 2：所有环境加载插件，其他情况不加载
        inject: false, // false: 不注入启动loader代码， 'all'： 每个入口都注入loader， entry：  每个html入口都注入自动运行loader
        name: "main", // 当前项目名称 默认 main,
        pages: String, // 自定义 页面入口 [name].js：是手动指定路人配置入口， src/file: 自动生成入口 仅过滤.vue 文件， 排除component[s] 文件
        version: 'version', //此次编译版本
        isMain: false,  // 当前项目是否主模块,
        html: true, // 多入口自动生成html
        //  默认项目配置 其他参数参考项目配置
        default: {
            publicPath: "", // 资源路径
            versionApi: "", // 版本控制url
            commonChunks: [], // [chunk-common, chunk-vendors],
            globalVersion: "baseConfig", // 针对 aff 框架 阻止运行
            stopGlobalVersion: true, // 针对 aff 框架 阻止运行,
            preload: false,
            bypass: Function | String, // bypass(chunkName, fileType, pathConfig) => url 自定义处理文件路径，
        },
        // 项目配置
        modules: [{
            mode: 0, // 0：开发环境启用，1：生产环境启用，0：所有环境启用，其他情况不加载
            name: "main", // 模块项目名称
            isMain: false, // 是否主项目（框架模块）
            isDevServe: false, // 是否开启开发环境,
            commonChunks: [], // [chunk-common, chunk-vendors],
            publicPath: "", // 资源路径
            versionApi: "", // 版本控制url
            globalVersion: "baseConfig", // 针对 aff 框架 阻止运行
            stopGlobalVersion: false, // 针对 aff 框架 阻止运行,
            // 加载指定的版本
            // 写死版本号 在非isDevServe 下有效果
            version: "version",
            entries: 'entries', // 自定义模块插件关系
            preload: false, // 是否启动预加载,
            bypass: Function | String, // 自定义处理文件路径，
            // 指定模块某些插件启用本配置
            pluginNames: []
        }]
    }
}
```
<!-- vue.config.js 配置 -->
```js
pluginOptions: {
    routeLoaderOptions: {
        name: "store",
        default: {
            publicPath: 'http://host/path',
            commonChunks: ['chunk-common', 'chunk-vendors.js'],
            versionApi: 'version.js',
        },
        modules: [
            // store 加载 mo 的本地机器代码
            {
                name: 'mo',
                publicPath: 'http://172.24.145.67:8899/',
                bypass: "static/{{fileType}}/{{chunkName}}!pluginNames=name1,name1,name1"
            }
        ]
    }
}
```
